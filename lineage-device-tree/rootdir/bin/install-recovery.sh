#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:83886080:57c2f0aeee7bd0dc3fa3ac312ac2fdb556df6dd8; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:bd13ccb5985515a41385a5f24d5278e456fa6cd9 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:83886080:57c2f0aeee7bd0dc3fa3ac312ac2fdb556df6dd8 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
