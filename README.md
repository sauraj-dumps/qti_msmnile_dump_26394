## qssi-user 11 RKQ1.200928.002 root05280121 release-keys
- Manufacturer: qualcomm
- Platform: msmnile
- Codename: msmnile
- Brand: qti
- Flavor: qssi-user
- Release Version: 11
- Kernel Version: 4.14.190
- Id: RKQ1.200928.002
- Incremental: root05280121
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/msmnile/msmnile:11/RKQ1.200928.002/root05280121:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200928.002-root05280121-release-keys
- Repo: qti_msmnile_dump_26394
